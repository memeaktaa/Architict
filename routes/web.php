<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['middleware' => 'web', 'prefix' => LaravelLocalization::setLocale()], function () {
    Auth::routes();
    Route::get('logout', 'Auth\LoginController@logout');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/', 'HomeController@index')->name('/');
    Route::get('page/{page}', 'HomeController@page')->name('page');
    Route::post('contact', 'HomeController@submitContact');
    Route::get('pass', function () {
        return bcrypt('omarmansour');
    });
    Route::get('new-work', function () {
        return view('pages.work-new');
    });
    Route::get('projects', function (\Illuminate\Http\Request $request) {
        $projects = \App\Models\Project::where('id','>',$request->get('offset'))->paginate(10);
        foreach ($projects as $key=>$project) {
            $data[] = ['name' => $project['title_' . App::getLocale()], 'link' => asset(\App\Models\Project::IMAGE_File_PATH . $project->image), 'id' => $project->id];
        }
        return $data;
    });
});
