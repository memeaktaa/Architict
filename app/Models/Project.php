<?php

namespace App\Models;

class Project extends \Eloquent
{
    protected $fillable = ['image','title_en','title_tr' ,'content_tr', 'content_en','location'];
    const IMAGE_URL_PATH = 'images/projects/';
    const IMAGE_File_PATH = 'storage/images/projects/';

    public function getImageFileSystem()
    {
        return storage_path('app/public/' . self::IMAGE_File_PATH . $this->image);
    }


}
