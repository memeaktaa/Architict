<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\General\Entities\GenSocialNetwork;
use Modules\Staff\Entities\Staff;

class Branch extends Model
{
    use SoftDeletes;
    protected $primaryKey="branch_id";
    protected $fillable = ['utc_offset','phone','mobile','email','address'];
}
