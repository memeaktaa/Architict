<?php

namespace App\Models;


class Tracker extends \Eloquent {


    protected $fillable = [ 'ip', 'date' ];

    public static function hit() {
        static::firstOrCreate([
            'ip'   => $_SERVER['REMOTE_ADDR'],
            'date' => date('Y-m-d H:i:s'),
        ])->save();
    }

}
