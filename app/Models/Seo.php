<?php

namespace App\Models;


class Seo extends \Eloquent
{
    protected $fillable=['title_en','title_tr','keyword_en','description_en','description_tr','keyword_tr'];
}
