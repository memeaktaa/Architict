<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactMail extends \Eloquent
{
    protected $fillable=['name','email','subject','message'];
}
