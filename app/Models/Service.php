<?php

namespace App\Models;

class Service extends \Eloquent
{
    protected $fillable = ['title_en','title_tr'];
}
