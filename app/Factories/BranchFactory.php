<?php

namespace App\Factories;

use App\Models\Team;
use Aut\DataTable\Factories\GlobalFactory;

class BranchFactory extends GlobalFactory
{

    /**
     *  get datatable query
     */
    public function getDatatable($model, $request)
    {
        $query = $model::all();
        return \Datatable::queryConfig('branches')
            ->queryDatatable($query)
            ->queryUpdateButton('branch_id')
            ->queryDeleteButton('branch_id')
            ->queryRender();
    }

    /**
     *  build datatable modal and table
     */
    public function buildDatatable($model, $request)
    {
        return \Datatable::config('branches','',['gridSystem'=>true,'dialogWidth'=>'60%'])
            ->addHiddenInput('branch_id', 'branch_id', '', true)
            ->addInputText(trans('app.email'), 'email', 'email','req required')
            ->addInputText(trans('app.phone'), 'phone', 'phone','req required')
            ->addInputText(trans('app.mobile'), 'mobile', 'mobile','req required')
            ->addInputText(trans('app.address'), 'address', 'address','req required')
            ->addInputText(trans('app.utc_offset'), 'utc_offset', 'utc_offset','req required')
            ->addActionButton($this->update, 'update', 'update')
            ->addActionButton($this->delete, 'delete', 'delete')
            ->addNavButton([],['code'])
            ->render();

    }

    /**
     *  store action for save relation
     */
    public function storeDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  store action for update relation
     */
    public function updateDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  store action for destroy relation
     */
    public function destroyDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  inline validate dialog form
     */
    public function validateDatatable()
    {
        return [];
    }
}
