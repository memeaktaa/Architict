<?php

namespace App\Factories;

use App\Models\Team;
use Aut\DataTable\Factories\GlobalFactory;

class TeamsFactory extends GlobalFactory
{

    /**
     *  get datatable query
     */
    public function getDatatable($model, $request)
    {
        $query = $model::all();
        return \Datatable::queryConfig('team')
            ->queryDatatable($query)
            ->queryUpdateButton('team_id')
            ->queryDeleteButton('team_id')
            ->queryCustomButton('update_image', 'team_id', 'fa fa-image', '', "href='javascript:void(0)' onclick='admin_update_image(this)'")
            ->queryAddColumn('image',function ($item) use($model){
                return $item->image?'<img src="' . ($item->image ? asset(Team::IMAGE_File_PATH . $item->image) : '') . '" />':'';
            })
            ->queryRender();
    }

    /**
     *  build datatable modal and table
     */
    public function buildDatatable($model, $request)
    {
        try {
            return \Datatable::config('team', '', ['gridSystem' => true, 'dialogWidth' => '60%'])
                ->addHiddenInput('team_id', 'team_id', '', true)
                ->addInputText(trans('app.name_en'), 'name_en', 'name_en', 'en d:en req required')
                ->addInputText(trans('app.name') . ' ' . trans('app._tr'), 'name_tr', 'name_tr', 'tr d:tr req required')
                ->addInputText(trans('app.job_title_en'), 'job_title_en', 'job_title_en', 'en d:en req required')
                ->addInputText(trans('app.job_title') . ' ' . trans('app._tr'), 'job_title_tr', 'job_title_tr', 'tr d:tr req required')
                ->setGridNormalCol(12)
                ->startHorizontalTab()
                ->openHorizontalTab('about1', trans('app.about') . ' ' . trans('app._en'), '', true)
                ->addTextArea(trans('app.about') . ' ' . trans('app._en'), 'about_en', 'about_en', 'en d:en d:text-editor none')
                ->closeHorizontalTab()
                ->openHorizontalTab('about2', trans('app.about') . ' ' . trans('app._tr'), '')
                ->addTextArea(trans('app.about') . ' ' . trans('app._tr'), 'about_tr', 'about_tr', 'tr d:tr d:text-editor none')
                ->closeHorizontalTab()
                ->endHorizontalTab()
                ->addInputText(trans('app.facebook'), 'facebook', 'facebook', 'req required')
                ->addInputText(trans('app.google'), 'google', 'google', 'req required')
                ->addInputText(trans('app.linkedin'), 'linkedin', 'linkedin', 'req required')
                ->addInputText(trans('app.team_order'), 'team_order', 'team_order', 'req required')
                ->addViewField(trans('app.image'), 'image', 'image', '', 'none')
                ->addActionButton(trans('app.update_image'), 'update_image', 'update_image')
                ->addActionButton($this->update, 'update', 'update')
                ->addActionButton($this->delete, 'delete', 'delete')
                ->addNavButton([], ['code'])
                ->onModalOpen('_textEditor($(modal))')
                ->render();
        } catch (\Exception $e) {
        }

    }

    /**
     *  store action for save relation
     */
    public function storeDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  store action for update relation
     */
    public function updateDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  store action for destroy relation
     */
    public function destroyDatatable($model = null, $request = null, $result = null)
    {
        //
    }

    /**
     *  inline validate dialog form
     */
    public function validateDatatable()
    {
        return [];
    }
}
