<?php

namespace App\Factories;

use App\Models\Page;
use App\Models\Product;
use App\Models\Seo;
use Aut\DataTable\Factories\GlobalFactory;

class PageFactory extends GlobalFactory
{

    /**
     *  get datatable query
     */
    public function getDatatable($model, $request)
    {
        $query = $model::with(['attribute','seo']);
        return \Datatable::queryConfig('pages')
            ->queryDatatable($query)
            ->queryUpdateButton('page_id')
            ->queryDeleteButton('page_id')
            ->queryCustomButton('attributes','page_id','icon-list',''," onClick='open_attributes_modal($(this))' href='javascript:void(0);'")
            ->queryAddColumn('attribute',function ($item){
                $ul='<ul>';
                foreach ($item->attribute as $value){
                    $ul .='<li>'.$value->text.': '. $value->pivot->attribute_value .'</li>';
                }
                $ul.='</ul>';
                return $ul;
            })
            ->queryRender();
    }

    /**
     *  build datatable modal and table
     */
    public function buildDatatable($model, $request)
    {
        try {
            return \Datatable::config('pages','',['gridSystem' => true, 'withTab' => true,'dialogWidth'=>'60%'])
                ->startTab(trans('app.page'), 'icon-docs')
                    ->addHiddenInput('page_id', 'page_id', '', true)
                    ->addInputText(trans('app.page'), 'page_name', 'page_name', '')
                ->endTab()
                ->startTab(trans('app.seo'), 'icon-globe-alt')
                ->startRelation('seo')
                ->addInputText(trans('app.title') . ' ' . trans('app._en'), 'seo.title_en', 'title_en', 'en d:en req required','','',false)
                ->addInputText(trans('app.title') . ' ' . trans('app._tr'), 'seo.title_tr', 'title_tr', 'tr d:tr req required','','',false)
                ->addInputText(trans('app.keyword') . ' ' . trans('app._en'), 'seo.keyword_en', 'keyword_en', 'en d:en req required','','',false)
                ->addInputText(trans('app.keyword') . ' ' . trans('app._tr'), 'seo.keyword_tr', 'keyword_tr', 'tr d:tr req required','','',false)
                ->setGridNormalCol(12)
                ->startHorizontalTab()
                ->openHorizontalTab('description1', trans('app.description_en') , 'req required', true)
                ->addTextArea(false, 'seo.description_en', 'description_en', 'en d:en none d:text-editor','','',false)
                ->closeHorizontalTab()
                ->openHorizontalTab('description2', trans('app.description').' '.trans('app._tr'), 'req required')
                ->addTextArea(false, 'seo.description_tr', 'description_tr', 'tr d:tr none d:text-editor','','',false)
                ->closeHorizontalTab()
                ->endHorizontalTab()
                ->endRelation()
                ->endTab()
                ->addViewField(trans('app.attributes'),'attribute','attribute','','none')
                ->addActionButton(trans('app.attributes'), 'attributes', 'attributes')
                ->addActionButton($this->update, 'update', 'update')
                ->addActionButton($this->delete, 'delete', 'delete')
                ->addNavButton([],['code'])
                ->onModalOpen('_textEditor($(modal))')
                ->render();
        } catch (\Exception $e) {
        }
    }

    /**
     *  store action for save relation
     */
    public function storeDatatable($model = null, $request = null, $result = null)
    {
        $seo = Seo::create(request()->input('seo'));
        $request['seo_id'] = $seo->id;
        $page = Page::create($request->input());
    }

    /**
     *  store action for update relation
     */
    public function updateDatatable($model = null, $request = null, $result = null)
    {
        $page=Page::findOrFail($request->page_id);
        $page->update($request->all());
        $page->seo()->update($request->input('seo'));
    }

    /**
     *  store action for destroy relation
     */
    public function destroyDatatable($model = null, $request = null, $result = null)
    {
        $id=$request['id'];
        $page=Page::findOrFail($id);
        $page->delete();
    }

    /**
     *  inline validate dialog form
     */
    public function validateDatatable()
    {
        return [];
    }
}
