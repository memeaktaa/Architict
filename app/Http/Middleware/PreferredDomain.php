<?php

namespace App\Http\Middleware;

use Closure;
use Redirect;

class PreferredDomain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!starts_with($request->header('host'), 'www.')) {
            $host =  'www.'.$request->header('host');
            $request->headers->set('host', $host);
            return Redirect::to($request->fullUrl(), 301);
        }

        return $next($request);
    }
}
