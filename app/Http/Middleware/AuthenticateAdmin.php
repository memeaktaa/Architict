<?php

namespace App\Http\Middleware;

use Closure;

class AuthenticateAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = '')
    {
        if (\Auth::check()) {
            return redirect()->route('table','generals');
        } else {
            return $next($request);
        }
    }
}
