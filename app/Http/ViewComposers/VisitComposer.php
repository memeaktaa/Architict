<?php

namespace App\Http\ViewComposers;

/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 12/31/2016
 * Time: 10:33 PM
 */
use App\Models\CmpGeneral;
use App\Models\Tracker;
use Illuminate\Contracts\View\View;
use LaravelLocalization;

class VisitComposer
{
    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $track = Tracker::where('ip', $_SERVER['REMOTE_ADDR'])->first();

        if (!$track){
            Tracker::hit();
        }
    }
}