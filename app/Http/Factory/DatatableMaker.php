<?php

namespace App\Http\Factory;

use Aut\DataTable\DataTableBuilder;
use Aut\DataTable\Http\Maker\BaseDataTableMaker;

use DB;

class DatatableMaker
{
    // use CustomDatatableMaker;

    protected $table;
    protected $yesNo;
    protected $update;
    protected $delete;
    protected $add;

    public function __construct(DataTableBuilder $table)
    {
        $this->table = $table;
        $this->update = trans('app.update');
        $this->delete = trans('app.delete');
        $this->add = trans('app.add');
        $this->yesNo = [
            'Y' => trans('admin::main.yes'),
            'N' => trans('admin::main.no')
        ];
    }
    /**
     * --------------------------------------
     *             example Table
     * --------------------------------------
     */

//    public function get_example_table($model, $request)
//    {
//        $query = DB::table('examples')->select([
//            'examples.example_id as id',
//            'examples.example_name_en as name_en',
//            'examples.example_name_ar as name_ar',
//            'examples.example_code as code',
//        ])
//        ->whereNull('examples.deleted_at');
//
//        return $this->table
//            ->queryConfig('datatable-example')
//            ->queryDatatable($query)
//            ->queryUpdateButton('id')
//            ->queryDeleteButton('id')
//            ->queryRender(true);
//    }
//
//    public function build_example_table($model, $request)
//    {
//        return $this->table
//            ->config('datatable-example',trans('admin::app.title_example'))
//            ->addHiddenInput('id','examples.example_id','',true)
//            ->addInputText('title_example_name_en','name_en','examples.example_name_en','en required req')
//            ->addInputText('title_example_name_ar','name_ar','examples.example_name_en','ar required req')
//            ->addInputText(trans('gen.code'),'code','examples.example_code','required req','maxlength="3"')
//            ->addActionButton($this->update,'update','update')
//            ->addActionButton($this->delete,'delete','delete')
//            ->addNavButton()
//            ->render();
//    }

    public function get_users_table($model, $request)
    {
        $query = $model::all();

        return $this->table
            ->queryConfig('users')
            ->queryDatatable($query)
            ->queryUpdateButton('id')
            ->queryDeleteButton('id')
            ->queryRender(true);
    }

    public function build_users_table($model, $request)
    {
        return $this->table
            ->config('users')
            ->addHiddenInput('id', 'id', '', true)

            ->addInputText('Name', 'name', 'name', ' required req')
            ->addInputText('Email', 'email', 'email', ' required req')
            ->addInputPassword('Password', 'password', 'password', 'none')
            ->addInputText('University ID', 'university_id', 'university_id', ' required req')
            ->addActionButton($this->update, 'update', 'update')
            ->addActionButton($this->delete, 'delete', 'delete')
            ->addNavButton()
            ->render();
    }
}
