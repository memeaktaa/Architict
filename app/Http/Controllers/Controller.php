<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Arcanedev\SeoHelper\Entities\Description;
use Arcanedev\SeoHelper\Entities\Keywords;
use Arcanedev\SeoHelper\Entities\OpenGraph\Graph;
use Arcanedev\SeoHelper\Entities\Title;
use Arcanedev\SeoHelper\Entities\Twitter\Card;
use Arcanedev\SeoHelper\Entities\Webmasters;
use http\Url;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Routing\Route;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $lang;

    public function __construct(Route $route)
    {
        $this->lang = \LaravelLocalization::getCurrentLocale();
        if (!\Request::ajax())
        $this->init($route);
    }

    public function init(Route $route)
    {
        if (\URL::current()==\route('home'))
        $pageName = 'home';
        else
        $pageName = \Request::route()->parameter('page');
        if(!$pageName){
            return;
        }
        $page = Page::where('page_name', $pageName)->with(['seo'])->first();
        if (count($page) == 0) {
            return abort(404);
        }
        view()->share('pageName',trans('app.'.$pageName));
        $this->SEO(array('title' => $page->seo['title_' . $this->lang] ? $page->seo['title_' . $this->lang] : trans('app.name'), 'description' => $page->seo['description_' . $this->lang] ? $page->seo['description_' . $this->lang] : ' ', 'keyword' => $page->seo['keyword_' . $this->lang] ? $page->seo['keyword_' . $this->lang] : ' ', 'pageName' => $pageName));

    }

    //---------------------------------------------------//
    /*
    *
    * @param bool $print used becuase of overwrite
    */
    public function SEO($seo = array(), $print = false)
    {
        $print && dd($seo);
        $title = new Title;
        $title->setSiteName(trans('app.name'));
        $title->set($seo['title']);
        $descriptionStr = isset($seo['description']) && $seo['description'] ? $seo['description'] : trans('app.footer_statement');
        $description = Description::make($descriptionStr)->render();
        $keyword = (isset($seo['keyword'])) ? Keywords::make($seo['keyword'])->render() : '';

        $tag['title'] = $title->render();
        $tag['description'] = $description;
        $tag['keyword'] = $keyword;
        view()->share('tag', $tag);

        if (isset($seo['pageName'])) {

            $titleStr = $seo['title'];//trans('app.name').($seo['pageName']!='home'?' - '.$seo['title']:'');


            $this->initTwitterCard($titleStr, $descriptionStr, $seo['pageName']);

            $this->initOpenGraph($titleStr, $descriptionStr, $seo['pageName']);

            $this->initWebMaster();
        }
    }

    private function initTwitterCard($title, $description, $pageName)
    {
        $card = new Card;
        $card->setType('summary_large_image');
        $card->setSite(config('seo-helper.twitter.site'));
        $card->addMeta('creator', config('seo-helper.twitter.meta.creator'));
        $card->setTitle($title ? $title : config('seo-helper.twitter.title'));
        $card->setDescription($description);
        $card->addMeta('url', config('seo-helper.twitter.metas.url'));
        $card->addImage(/*File::exists(public_path('images/seo/' . $pageName . '.jpg')) ?
            asset('images/seo/' . $pageName . '.jpg') : File::exists(public_path('images/seo/' . $pageName . '.png')) ?
                asset('images/seo/' . $pageName . '.png') :*/
            config('seo-helper.twitter.image'));
        view()->share('card', $card->render());

    }

    private function initOpenGraph($title, $description, $pageName)
    {
        $openGraph = new Graph([
            'prefix' => config('seo-helper.open-graph.prefix'),
            'type' => config('seo-helper.open-graph.website'),
            'title' => $title ? $title : config('seo-helper.open-graph.title'),
            'description' => $description ? $description : config('seo-helper.open-graph.description'),
            'site-name' => config('seo-helper.open-graph.site-name'),
            'properties' => [
                'profile:username' => config('seo-helper.open-graph.properties.profile:username'),
                'image:width' => config('seo-helper.open-graph.properties.image:width'),
                'image:height' => config('seo-helper.open-graph.properties.image:height'),
                'image' => /*File::exists(public_path('images/seo/' . $pageName . '.jpg')) ?
                    asset('images/seo/' . $pageName . '.jpg') : File::exists(public_path('images/seo/' . $pageName . '.png')) ?
                        asset('images/seo/' . $pageName . '.png') :*/
                    config('seo-helper.open-graph.properties.image'),

            ],
        ]);
        view()->share('openGraph', $openGraph->render());
    }

    private function initWebMaster()
    {

        $webmasters = Webmasters::make([
            'google' => config('seo-helper.webmasters.google'),
            'bing' => config('seo-helper.webmasters.bing'),
            'alexa' => config('seo-helper.webmasters.alexa'),
            'pinterest' => config('seo-helper.webmasters.pinterest'),
            'yandex' => config('seo-helper.webmasters.yandex')
        ]);

        view()->share('webmasters', $webmasters->render());
    }
}
