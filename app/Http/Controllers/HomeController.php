<?php

namespace App\Http\Controllers;

use App\Models\Attribute;
use App\Models\ContactMail;
use App\Models\Gallery;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Mail;
use App\Mail\sendEmail;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gallery = Gallery::all();
        return view('home', compact('gallery'));
    }

    public function page($page)
    {
        $info = [];
        $pageContent = Page::where('page_name', $page)->with(['attributePage.attribute'])->first();
        $attrs = Attribute::all();
        if (isset($pageContent->attributePage)) {
            foreach ($pageContent->attributePage as $item) {
                foreach ($attrs as $attr) {
                    if ($item->attribute->text == $attr->text)
                        $info[$item->attribute->text] = [$item->attribute->text => $item->attribute_value];
                }
            }
            foreach ($attrs as $attr) {
                if (isset($info[$attr->text])) {
                    $obj = $info[$attr->text][$attr->text];
                    $info[$attr->text] = $obj;
                }
            }
        }
        return view('pages.' . $page, compact('pageContent', 'info'));
    }

    /**
     * @param Request $request
     * @return array
     */
    public function submitContact(Request $request)
    {
        $contact = ContactMail::create($request->input());
        $emails = ['oma.design.group@gmail.com',/*, 'fidel@oma-designgroup.com',
            'hani@oma-designgroup.com', 'haysam@oma-designgroup.com',
            'muhammad@oma-designgroup.com',*/ 'omar@oma-designgroup.com'
           ];
        foreach ($emails as $email) {

            Mail::send('email.email', ['title' => '', 'user' => $request->get('email'), 'subject' => $request->get('subject'), 'content' => $request->get('message')], function ($message) use ($request,$email) {
                $message->from('oma.design@oma-designgroup.com', $request->get('name'));
                $message->subject($request->get('subject'));
                $message->to($email);
//                    Mail::to($email)->send(new sendEmail('oma.design@oma-designgroup.com', $request->get('subject'), $request->get('message'), $request->get('name')));
            });
        }
        return ['success' => 'success', 'msg' => trans('app.sent_success')];
    }

}
