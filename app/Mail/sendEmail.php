<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $user,$subject,$content,$name;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$subject,$content,$name)
    {
        $this->user=$user;
        $this->subject=$subject;
        $this->content=$content;
        $this->name=$name;
    }

    public function build()

    {
        return $this->view('email.email',['content'=>$this->content])
            ->from($this->user, $this->name)
            ->subject($this->subject);
    }
}
