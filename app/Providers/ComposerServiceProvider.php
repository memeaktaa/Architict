<?php

namespace App\Providers;

use App\Http\ViewComposers\GlobalComposer;
use App\Http\ViewComposers\VisitComposer;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(
            '*', GlobalComposer::class
        );
        view()->composer(
            'home', VisitComposer::class
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
