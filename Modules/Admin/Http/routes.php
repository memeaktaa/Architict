<?php

Route::group(['middleware' => ['web'], 'prefix' => LaravelLocalization::setLocale() . '/admin', 'namespace' => 'Modules\Admin\Http\Controllers'], function () {
    Route::get('table/{table}', 'AdminController@table')->name('table');
    Route::get('/', 'AdminController@index');
    Route::post('upload-image/{resize?}', 'AdminController@uploadImage');
    Route::post('upload-doc/{file}', 'AdminController@uploadDocument');
    Route::put('gallery/{id}', 'AdminController@updateImageGallery');
    Route::put('projects/{id}', 'AdminController@updateImageProjects');
    Route::put('team/{id}', 'AdminController@updateImageTeam');
    Route::put('generals/{id}', 'AdminController@updateImageGeneral');
    Route::put('clients/{id}', 'AdminController@updateImageClients');
    Route::get('attributes-autocomplete', 'AdminController@attributeAutocomplete');
    Route::post('delete-visitors',function (){
       \App\Models\Tracker::truncate();
       return Response::json(['msg'=>trans('admin::main.deleted_success')]);
    });
    Route::get('get-visits', function () {
        $labels = ['1' => 'Jan', '2' => 'Feb', '3' => 'Mar', '4' => 'Apr', '5' => 'May', '6' => 'Jun', '7' => 'Jul', '8' => 'Aug', '9' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec'];
        $visits = \App\Models\Tracker::orderBy('date', 'desc')->get();
        $data = [];
        foreach ($labels as $key => $label) {
            foreach ($visits as $visit) {
                if (date('m', strtotime($visit->date)) == $key) {
                    $visit->month = $label;
                    $visit->count = \App\Models\Tracker::whereMonth('date', '=', $key)->get()->count();
                    $data[$key]['ip'] = $visit->ip;
                    $data[$key]['month'] = $visit->month;
                    $data[$key]['count'] = $visit->count;
                    $data[$key]['date'] = date('Y-M', strtotime($visit->date));
                }
            }

        }
        return $data;
    });
});
