<?php

namespace Modules\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Attribute_page;
use App\Models\Attribute;
use App\Models\Client;
use App\Models\CmpGeneral;
use App\Models\Gallery;
use App\Models\Project;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Intervention\Image\ImageManager;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admin::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    public function table($table, $hasExtra = false)
    {
        $tableName = $table . '-table';
        return view('admin::table', compact('tableName', 'title', 'hasExtra', 'table'));
    }

    function moveFileFromTemp($newPath, $name, $newName = '')
    {
        if ($newName == '') $newName = $name;
        \File::move(storage_path('app/public/temp/' . $name), storage_path('app/public/' . $newPath) . $newName);
    }

    public function uploadImage(Request $request, $resize = false)
    {
//        list($width, $height) = getimagesize("Full path of the image");
        $this->validate($request, [
            'image' => 'mimes:jpeg,jpg,bmp,png,mp4,avi,flv|max:1000000',
            'upload' => 'image|max:1000000',//For CKEditor
            'image_url' => 'max:10000000',//For Crop
        ]);
        $manager = new ImageManager(array('driver' => 'gd'));
        $image = $manager->make($request->files->get('image', $request->get('image_url', $request->get('upload', ''))));
        if ($resize == 'image') {
            $resize = $image->resize(247, 179);
            $fileTempName = $request->get('name') ? $request->get('name') . sha1(time() . (int)rand(10, 100)) . '.' . explode('/', $resize->mime())[1] : $request->get('image') . sha1(time() . (int)rand(10, 100)) . '.' . explode('/', $resize->mime())[1];
        } else {
            $fileTempName = $request->get('name') ? $request->get('name') . sha1(time() . (int)rand(10, 100)) . '.' . explode('/', $image->mime())[1] : $request->get('image') . sha1(time() . (int)rand(10, 100)) . '.' . explode('/', $image->mime())[1];
        }
        $image->save(storage_path('app/public/temp/' . $fileTempName));
        return ['success' => true, 'filename' => $fileTempName, 'fileurl' => url('storage/temp/' . $fileTempName)];
    }

    public function updateImageGallery($id, Request $request)
    {
        $gallery = Gallery::find($id);
        if ($request->get('image') != $gallery->image) {
            \File::delete($gallery->getImageFileSystem());
            $this->moveFileFromTemp(Gallery::IMAGE_URL_PATH, $request->get('image'));
        }
        $gallery->update($request->input());
    }

    public function updateImageProjects($id, Request $request)
    {
        $project = Project::find($id);
        if ($request->get('image') != $project->image) {
            \File::delete($project->getImageFileSystem());
            $this->moveFileFromTemp(Project::IMAGE_URL_PATH, $request->get('image'));
        }
        $project->update($request->input());
    }

    public function updateImageTeam($id, Request $request)
    {
        $team = Team::find($id);
        if ($request->get('image') != $team->image) {
            \File::delete($team->getImageFileSystem());
            $this->moveFileFromTemp(Team::IMAGE_URL_PATH, $request->get('image'));
        }
        $team->image = $request->get('image');
        $team->save();
    }

    public function updateImageClients($id, Request $request)
    {
        $client = Client::find($id);
        if ($request->get('logo') != $client->logo) {
            \File::delete($client->getImageFileSystem());
            $this->moveFileFromTemp(Client::LOGO_URL_PATH, $request->get('logo'));
        }
        $client->logo = $request->get('logo');
        $client->save();
    }

    public function updateImageGeneral($id, Request $request)
    {
        $general = CmpGeneral::find($id);
        if ($request->get('image') != $general->value) {
            \File::delete($general->getImageFileSystem());
            $this->moveFileFromTemp(CmpGeneral::IMAGE_URL_PATH, $request->get('image'));
        }
        $general->value = $request->get('image');
        $general->save();
    }

    public function updateImageAttributePage($id, Request $request)
    {
        $attr_page = Attribute_page::find($id);
        if ($request->get('image') != $attr_page->attribute_value) {
            \File::delete($attr_page->getImageFileSystem());
            $this->moveFileFromTemp(Attribute_page::IMAGE_URL_PATH, $request->get('image'));
        }
        $attr_page->attribute_value = $request->get('image');
        $attr_page->save();
    }

    public function attributeAutocomplete(Request $request)
    {
        $q = str_replace(' ', '%', $request->get('q', ''));
        $data = Attribute::whereRaw('upper(text) like upper(\'%' . $q . '%\')')
            ->get(['text as name', 'attribute_id as id']);
        return ['items' => $data];
    }
}
