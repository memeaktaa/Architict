@extends('admin::layouts.app')

@section('content')
    <section>
        <!-- Page content-->
        <div class="content-wrapper">
            <div class="row">
                <div class="col-lg-4">
                    <!-- START List group-->
                    <ul class="list-group">
                        <li class="list-group-item">
                            <div class="row row-table pv-lg">
                                <div class="col-xs-6">
                                    <p class="m0 lead">1204</p>
                                    <p class="m0">
                                        <small>Commits this month</small>
                                    </p>
                                </div>
                                <div class="col-xs-6 text-center">
                                    <div data-sparkline="" data-bar-color="#23b7e5" data-height="60" data-bar-width="10" data-bar-spacing="6" data-chart-range-min="0" values="3,6,7,8,4,5"></div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row row-table pv-lg">
                                <div class="col-xs-6">
                                    <p class="m0 lead">$ 3,200.00</p>
                                    <p class="m0">
                                        <small>Available budget</small>
                                    </p>
                                </div>
                                <div class="col-xs-6 text-center">
                                    <div data-sparkline="" data-type="line" data-height="60" data-width="80%" data-line-width="2" data-line-color="#7266ba" data-chart-range-min="0" data-spot-color="#888" data-min-spot-color="#7266ba" data-max-spot-color="#7266ba"
                                         data-fill-color="" data-highlight-line-color="#fff" data-spot-radius="3" values="7,3,4,7,5,9,4,4,7,5,9,6,4" data-resize="true"></div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row row-table pv-lg">
                                <div class="col-xs-6">
                                    <p class="m0 lead">67</p>
                                    <p class="m0">
                                        <small>New followers</small>
                                    </p>
                                </div>
                                <div class="col-xs-6">
                                    <ul class="list-inline text-center">
                                        <li>
                                            <a href="#" tooltip="Katie">
                                                <img src="img/user/02.jpg" alt="Follower" class="img-responsive img-circle thumb24">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" tooltip="Cody">
                                                <img src="img/user/01.jpg" alt="Follower" class="img-responsive img-circle thumb24">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" tooltip="Tamara">
                                                <img src="img/user/03.jpg" alt="Follower" class="img-responsive img-circle thumb24">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" tooltip="Gene">
                                                <img src="img/user/04.jpg" alt="Follower" class="img-responsive img-circle thumb24">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" tooltip="Marsha">
                                                <img src="img/user/04.jpg" alt="Follower" class="img-responsive img-circle thumb24">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" tooltip="Robin">
                                                <img src="img/user/09.jpg" alt="Follower" class="img-responsive img-circle thumb24">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <!-- END List group-->
                </div></div>
                </div>
    </section>

@stop
