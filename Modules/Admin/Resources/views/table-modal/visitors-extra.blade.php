<script>
    function _deleteAll($this) {
        var url=baseUrl+'admin/delete-visitors';
        aut_datatable_swal({
            title: '{{trans('app.sweet.msg_delete')}}',
            type: "error",
            confirmButtonText: '{{trans('app.sweet.ok')}}',
            cancelButtonText: '{{trans('app.sweet.cancel')}}',
            showCancelButton: !0,
            showCloseButton: !0,
            allowEscapeKey: !0,
            allowOutsideClick: !0,
            confirmButtonColor: "#DD6B55",
            showLoaderOnConfirm: !0
        },function () {
            $.post(url,function (data) {
                aut_datatable_notify({message:data.msg,status:'info'});
                $datatable = $($this).closest(".datatable");
                $datatable.load($datatable.attr("data-url"));
            })
        })
    }
</script>