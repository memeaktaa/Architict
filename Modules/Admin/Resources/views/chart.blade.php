<div class="divider divider-center notopmargin divider-short"></div>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-body">
            <canvas id="line-chart" width="1140" height="400"></canvas>
        </div>
        <div class="panel-footer">
        <p class="text-center">Visitors per Month</p>
        </div>
    </div>
</div>