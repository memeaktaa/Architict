<header class="topnavbar-wrapper">
    <!-- START Top Navbar-->
    <nav role="navigation" class="navbar topnavbar">
        <!-- START navbar header-->
        <div class="navbar-header">
            <a href="{{localizeURL('/')}}" class="navbar-brand">
                <div class="brand-logo">
                    <img src="{{isset($logo)?asset(\App\Models\CmpGeneral::IMAGE_File_PATH.$logo->value):asset('images/logo.png')}}" alt="App Logo" class="img-responsive" style="height: 38px">
                </div>
                <div class="brand-logo-collapsed">
                    <img src="{{isset($logo)?asset(\App\Models\CmpGeneral::IMAGE_File_PATH.$logo->value):asset('images/logo.png')}}" alt="App Logo" class="img-responsive"
                         style="margin-top: 22%">
                </div>
            </a>
        </div>
        <div class="nav-wrapper">
            <ul class="nav navbar-nav" style="">
                <li>
                    <a id="side-open" href="#" data-trigger-resize="" data-toggle-state="aside-collapsed"
                       class="hidden-xs">
                        <em class="fa fa-navicon"></em>
                    </a>
                    <a href="#" data-toggle-state="aside-toggled" data-no-persist="true"
                       class="visible-xs sidebar-toggle">
                        <em class="fa fa-navicon"></em>
                    </a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                {{--<li class="">--}}
                    {{--<a href="{{route('table',['table'=>'all-requests'])}}">--}}
                        {{--<em class="fa fa-inbox" title="{{trans('app.requests')}}"><em class="badge"--}}
                                                                                      {{--style="background-color: #ff353e">{{\App\Models\Request::where('is_accepted','0')->count()}}</em></em>--}}
                    {{--</a>--}}
                {{--</li>--}}
                @if(Auth::check())
                    <li class="dropdown dropdown-list">
                        <a href="#" data-toggle="dropdown">
                            <em class="icon-user"></em>
                        </a>
                        <ul class="dropdown-menu animated flipInX" style=" font-family: 'messeri'">
                            <li>
                                <li class="list-group">
                                    <div class=""
                                         style="margin-top: 10px;text-align: center;">{{Auth::user()->name}}
                                    </div>
                                    <div class=""
                                         style="margin-top: 10px;text-align: center;">{{Auth::user()->email}}
                                    </div>
                                </li>
                                <li class="topmargin-sm text-center">
                                    <a href="{{localizeURL('logout')}}" class="btn btn-danger btn-block"
                                       style="margin: 10px 17px;width: 85%;"
                                       >{{trans('app.logout')}}</a>
                                </li>
                            </li>
                        </ul>
                    </li>
                @endif
                <li class="dropdown dropdown-list">
                    <a href="#" data-toggle="dropdown">
                        <img src="{{asset('images/icons/'.$lang.'.png')}}" style="width: 16px;height: 16px">
                    </a>
                    <ul class="dropdown-menu animated flipInX">
                        <li>
                            <div class="list-group">

                                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                    <a href="{{ LaravelLocalization::getLocalizedURL($localeCode) }}"
                                       hreflang="{{$localeCode}}" class="list-group-item">
                                        <div class="media-box">
                                            <div class="pull-left">
                                                <img src="{{asset('images/icons/'.$localeCode.'.png')}}" style="width: 16px;height: 16px">
                                            </div>
                                            <div class="media-box-body clearfix">
                                                <p class="m0">{{ $properties['native'] }}</p>

                                            </div>
                                        </div>
                                    </a>
                                @endforeach

                            </div>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" onClick="history.go(0)">
                        <em class="icon-refresh"></em>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>
{{--@if(Auth::check())--}}
<aside class="aside">
    <div class="aside-inner">
        <nav data-sidebar-anyclick-close="" class="sidebar">
            <ul class="nav" style="font-family: 'messeri'">
                <li class="nav-heading ">
                    <span data-localize="sidebar.heading.COMPONENTS">{{trans('app.settings')}}</span>
                </li>
                <li class=" ">
                    <a href="{{route('table',['table'=>'pages'])}}"
                       class="@if(URL::current()==route('table',['table'=>'pages'])){{ 'current' }}@endif ">
                        <em class="icon icon-docs"></em> <span>{{trans('app.pages')}}</span>
                    </a>
                </li>
                <li class=" ">
                    <a href="{{route('table',['table'=>'gallery'])}}"
                       class="@if(URL::current()==route('table',['table'=>'gallery'])){{ 'current' }}@endif ">
                        <em class="fa fa-image"></em> <span>{{trans('app.gallery')}}</span>
                    </a>
                </li>
                <li class=" ">
                    <a href="{{route('table',['table'=>'projects'])}}"
                       class="@if(URL::current()==route('table',['table'=>'projects'])){{ 'current' }}@endif ">
                        <em class="fa fa-book"></em> <span>{{trans('app.projects')}}</span>
                    </a>
                </li>
                <li class=" ">
                    <a href="{{route('table',['table'=>'contact-mails'])}}"
                       class="@if(URL::current()==route('table',['table'=>'contact-mails'])){{ 'current' }}@endif ">
                        <em class="icon icon-envelope-open"></em> <span>{{trans('app.contact-mails')}}</span>
                    </a>
                </li>
                <li class=" ">
                    <a href="{{route('table',['table'=>'generals'])}}"
                       class="@if(URL::current()==route('table',['table'=>'generals'])){{ 'current' }}@endif ">
                        <em class="fa fa-spinner"></em><span>{{trans('app.generals')}}</span>
                    </a>
                </li>
                <li class=" ">
                    <a href="{{route('table',['table'=>'branches'])}}"
                       class="@if(URL::current()==route('table',['table'=>'branches'])){{ 'current' }}@endif ">
                        <em class="icon icon-layers"></em><span>{{trans('app.branches')}}</span>
                    </a>
                </li>
                <li class=" ">
                    <a href="{{route('table',['table'=>'clients'])}}"
                       class="@if(URL::current()==route('table',['table'=>'clients'])){{ 'current' }}@endif ">
                        <em class="fa fa-users"></em><span>{{trans('app.clients')}}</span>
                    </a>
                </li>
                <li class=" ">
                    <a href="{{route('table',['table'=>'services'])}}"
                       class="@if(URL::current()==route('table',['table'=>'services'])){{ 'current' }}@endif ">
                        <em class="icon icon-grid"></em><span>{{trans('app.services')}}</span>
                    </a>
                </li>
                <li class=" ">
                    <a href="{{route('table',['table'=>'visitors'])}}"
                       class="@if(URL::current()==route('table',['table'=>'visitors'])){{ 'current' }}@endif ">
                        <em class="icon icon-users"></em><span>{{trans('app.visitors')}}</span>
                    </a>
                </li>
            </ul>
            <!-- END sidebar nav-->
        </nav>
    </div>
</aside>
{{--@endif--}}
