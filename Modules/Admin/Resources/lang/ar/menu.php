<?php

return [

    'events' => 'الفعاليات',
    'admins'=>'المدراء',
    'users'=>'المستخدمين',
    'contact_mails'=>'بريد التواصل',
    'general'=>'عام',
    'countries'=>'الدول',
    'states'=>'المحافظات',
    'cities'=>'المدن',
    'slider_items'=>'عناصر الشرائح',
    'learned_languages'=>'اللغات المتقنة',
    'tags'=>'الأصناف',
    'religions'=>'الديانات',
    'ideologies'=>'المذاهب',
    'races'=>'العروق',
    'locations'=>'الموقع',
    'civil_status'=>'الحالة المدنية',
    'educational_situations'=>'الخلفية التعليمية',
    'political_situations'=>'الخلفية السياسية',
];