<footer id="footer" class="">


    <!-- Copyrights
    ============================================= -->
    <div id="copyrights">

        <div class="container c-footer">


            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                {!! trans('app.copyright') !!}
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-8 socila" style="">
                @if($facebook)
                    <a href="https://www.facebook.com/{{$facebook->value}}" target="_blank"
                       class="social-icon si-rounded si-borderless icon-facebook"
                       style=";">
                        <div hidden>
                            <i class="icon-facebook" style="color:#575757;"></i>
                            <i class="icon-facebook"></i>
                        </div>
                    </a>
                @endif
                @if($linkedin)
                    <a href="https://www.linkedin.com/{{$linkedin->value}}" target="_blank"
                       class="social-icon si-rounded si-borderless icon-linkedin"
                       style=";">
                        <div hidden>
                            <i class="icon-linkedin" style="color:#575757;"></i>
                            <i class="icon-linkedin"></i>
                        </div>
                    </a>
                @endif
                @if($instagram)
                    <a href="https://www.instagram.com/{{$instagram->value}}" target="_blank"
                       class="social-icon si-rounded si-borderless icon-instagram"
                       style=";">
                        <div hidden>
                            <i class="icon-instagram" style="color:#575757;"></i>
                            <i class="icon-instagram"></i>
                        </div>
                    </a>
                @endif
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 develop" style="padding-left: 12%">
                Developed By Mohammad Aktaa
            </div>
        </div>
    </div><!-- #copyrights end -->

</footer>
