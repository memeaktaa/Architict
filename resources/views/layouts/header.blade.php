<header id="header" class="full-header transparent-header">

    <div id="header-wrap">

        <div class="container clearfix @if(URL::current()==route('home') || URL::current()==route('/')) t @else white-header @endif">

            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

            <!-- Logo
            ============================================= -->
            <div id="logo" style="border-right:none !important; ">
                <a href="{{route('home')}}" class="standard-logo"
                   style="margin-top: 10% !important;color: @if(URL::current()==route('home') ||URL::current()==route('/')) white @endif"><b>{{$project_name->value}}</b></a>
                <a href="{{route('home')}}" class="retina-logo"
                   style="margin-top: 5% ;color: @if(URL::current()==route('home') ||URL::current()==route('/')) white @endif"><b
                            class="project-name">{{$project_name->value}}</b></a>
            </div><!-- #logo end -->

            <!-- Primary Navigation
            ============================================= -->
            <nav id="primary-menu" class="dark">

                <ul class="@if(URL::current()==route('home') || URL::current()==route('/')) show @endif"
                    style="border-right: none !important;">

                    <li class=" " style="height: 40px">
                        @if($facebook)
                            <a href="https://www.facebook.com/{{$facebook->value}}" target="_blank"
                               class="social-icon si-rounded si-borderless icon-facebook"
                               style="padding: 2px 11px;margin: 32px 0px;">
                                <div hidden>
                                    <i class="icon-facebook" style="color:#575757;"></i>
                                    <i class="icon-facebook"></i>
                                </div>
                            </a>
                        @endif
                        @if($linkedin)
                            <a href="https://www.linkedin.com/{{$linkedin->value}}" target="_blank"
                               class="social-icon si-rounded si-borderless icon-linkedin2"
                               style="padding: 2px 11px;margin: 32px 0px;">
                                <div hidden>
                                    <i class="icon-linkedin" style="color:#575757;"></i>
                                    <i class="icon-linkedin"></i>
                                </div>
                            </a>
                        @endif
                        @if($instagram)
                            <a href="https://www.instagram.com/{{$instagram->value}}" target="_blank"
                               class="social-icon si-rounded si-borderless icon-instagram"
                               style="padding: 2px 11px;margin: 32px 0px;">
                                <div hidden>
                                    <i class="icon-instagram" style="color:#575757;"></i>
                                    <i class="icon-instagram"></i>
                                </div>
                            </a>
                        @endif
                    </li>

                    <li class=" "><a
                                class=""
                                href="{{route('/')}}"
                                style="color: @if(URL::current()==route('home') ||URL::current()==route('/')) white @else #0a1115 @endif;font-weight: 800">
                            <div>{{trans('app.home')}}</div>
                        </a>
                    </li>
                    <li class=" "><a class=""
                                     href="{{route('page',['page'=>'work'])}}"
                                     style="color: @if(URL::current()==route('home') ||URL::current()==route('/')) white @else #0a1115 @endif;font-weight: 800">
                            <div>{{trans('app.work')}}</div>
                        </a>
                    </li>
                    <li class=" "><a class=""
                                     href="{{route('page',['page'=>'about-us'])}}"
                                     style="color: @if(URL::current()==route('home') ||URL::current()==route('/')) white @else #0a1115 @endif;font-weight: 800">
                            <div>{{trans('app.about')}}</div>
                        </a>
                    </li>
                    <li class=" "><a class=""
                                     href="{{route('page',['page'=>'contact'])}}"
                                     style="color: @if(URL::current()==route('home') ||URL::current()==route('/')) white @else #0a1115 @endif;font-weight: 800">
                            <div>{{trans('app.contact')}}</div>
                        </a>
                    </li>
                    <li class=" "><a class="" href="#"
                                     {{--{{ LaravelLocalization::getLocalizedURL($lang=='en'?'tr':'en') }}--}}
                                     hreflang="{{ $lang=='en'?'TR':'EN' }}"
                                     style="color: @if(URL::current()==route('home') ||URL::current()==route('/')) white @else #0a1115 @endif;font-weight: 800">
                            <div>{{$lang=='en'?'TR':'EN'}}</div>
                        </a>
                    </li>
                    {{--<li class=""><a href="#">--}}
                    {{--<img src="{{ asset("images/icons/$lang.png") }}" alt="{{ $lang }} flag"--}}
                    {{--style="width: 16px;height: 16px;">--}}
                    {{--</a>--}}
                    {{--<ul>--}}
                    {{--@foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)--}}
                    {{--<li>--}}
                    {{--<a href="{{ LaravelLocalization::getLocalizedURL($localeCode) }}"--}}
                    {{--hreflang="{{ $localeCode }}">--}}
                    {{--<img src="{{ asset("images/icons/$localeCode.png") }}"--}}
                    {{--alt="{{ $properties['native'] }} flag"--}}
                    {{--style="width: 16px;height: 16px;"> {{ $properties['native'] }}--}}
                    {{--</a>--}}
                    {{--</li>--}}
                    {{--@endforeach--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                </ul>
            </nav><!-- #primary-menu end -->

        </div>

    </div>

</header>