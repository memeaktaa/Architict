<section id="slider" class="slider-parallax revslider-wrap clearfix " style="overflow-y: hidden">
    <div class="slider-parallax-inner">
        <div class="tp-banner-container" style="width: 100% !important;">
            <div class="tp-banner">
                <ul>    <!-- SLIDE  -->
                    @foreach($gallery as $item)
                        <li class="dark" data-transition="fadeInUp" data-slotamount="1" data-masterspeed="2500"
                            data-thumb="{{asset(\App\Models\Gallery::IMAGE_File_PATH.$item->image)}}" data-delay="9000"
                            data-saveperformance="on" data-title="Responsive Design">
                            <!-- MAIN IMAGE -->
                            <img src="{{asset(\App\Models\Gallery::IMAGE_File_PATH.$item->image)}}" alt="kenburns1"
                                 data-bgposition="center bottom" data-bgpositionend="center top" data-kenburns="on"
                                 data-duration="10000" data-ease="Linear.easeNone" data-scalestart="100"
                                 data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-blurstart="0"
                                 data-blurend="0" data-offsetstart="0 0" data-offsetend="0 0" class="rev-slidebg"
                                 data-no-retina>
                            <!-- LAYERS -->

                            <!-- LAYER NR. 2 -->
                            {{--<div class="tp-caption customin ltl tp-resizeme revo-slider-caps-text uppercase"--}}
                                 {{--data-x="480"--}}
                                 {{--data-y="300"--}}
                                 {{--data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"--}}
                                 {{--data-speed="800"--}}
                                 {{--data-start="1000"--}}
                                 {{--data-easing="easeOutQuad"--}}
                                 {{--data-splitin="none"--}}
                                 {{--data-splitout="none"--}}
                                 {{--data-elementdelay="0.01"--}}
                                 {{--data-endelementdelay="0.1"--}}
                                 {{--data-endspeed="1000"--}}
                                 {{--data-endeasing="Power4.easeIn"--}}
                                 {{--style="z-index: 3; white-space: nowrap;color: #0a1115 !important;font-family: 'omar';font-size: 25px">{!! $item['content_'.$lang] !!}</div>--}}

                            {{--<div class="tp-caption customin ltl tp-resizeme revo-slider-caps-text nopadding noborder"--}}
                                 {{--data-x="485"--}}
                                 {{--data-y="190"--}}
                                 {{--data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"--}}
                                 {{--data-speed="800"--}}
                                 {{--data-start="1200"--}}
                                 {{--data-easing="easeOutQuad"--}}
                                 {{--data-splitin="none"--}}
                                 {{--data-splitout="none"--}}
                                 {{--data-elementdelay="0.01"--}}
                                 {{--data-endelementdelay="0.1"--}}
                                 {{--data-endspeed="1000"--}}
                                 {{--data-endeasing="Power4.easeIn"--}}
                                 {{--style="z-index: 3; white-space: nowrap;color: #0a1115 !important; font-family: 'omar';width: 200px;">--}}
                                {{--<img src="{{asset(\App\Models\CmpGeneral::IMAGE_File_PATH.$logo->value)}}" height="200"--}}
                                     {{--width="300"--}}
                                     {{--alt="kenburns1" style="width: 0% !important;height: 35% !important;"/></div>--}}
                        </li>
                    @endforeach
                </ul>
            </div>
        </div><!-- END REVOLUTION SLIDER -->
    </div>
</section>
