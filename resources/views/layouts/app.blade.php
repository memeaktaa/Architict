<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="jxGXo6bzeDR_pJvde68ZEhV7JSUvha7SClnRTTxmIk8" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon"
          href="{{isset($favicon)?asset(\App\Models\CmpGeneral::IMAGE_File_PATH.$favicon->value):asset('images/favicon.png')}}"
          type="image/x-icon">
    <meta name="theme-color" content="#262626" />
    <title>{{isset($project_name)?$project_name->value:'OMA'}} {{--| @yield('title')--}}</title>

    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}" type="text/css"/>

    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" type="text/css"/>

    <link rel="stylesheet" href="{{ asset('css/style.css') }}" type="text/css"/>

    <link rel="stylesheet" href="{{ asset('css/swiper.css') }}" type="text/css"/>

    <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}" type="text/css"/>

    {{--<link rel="stylesheet" href="{{ asset('css/dark.css') }}" type="text/css"/>--}}

    <link rel="stylesheet" href="{{ asset('css/font-icons.css') }}" type="text/css"/>

    <link rel="stylesheet" href="{{ asset('css/animate.css') }}" type="text/css"/>

    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}" type="text/css"/>

    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}" type="text/css"/>

    <link rel="stylesheet" href="{{ asset('css/custom.css') }}" type="text/css"/>

    <link rel="stylesheet" href="{{ asset('css/gray.min.css') }}" type="text/css"/>
    <style>

        .revo-slider-emphasis-text {
            font-size: 64px;
            font-weight: 700;
            letter-spacing: -1px;
            font-family: 'Raleway', sans-serif;
            padding: 15px 20px;
            border-top: 2px solid #FFF;
            border-bottom: 2px solid #FFF;
        }

        .revo-slider-desc-text {
            font-size: 20px;
            font-family: 'Lato', sans-serif;
            width: 650px;
            text-align: center;
            line-height: 1.5;
        }

        .revo-slider-caps-text {
            font-size: 16px;
            font-weight: 400;
            letter-spacing: 3px;
            font-family: 'Raleway', sans-serif;
        }

        .revo-slider-caps-text > img {
            width: 200px !important;
            height: 130px !important;
        }

        .tp-video-play-button {
            display: none !important;
        }

        .tp-caption {
            white-space: nowrap;
        }

        @font-face {
            font-family: "omar";
            src: url('../../../css/fonts/EUROCAPS.ttf');
        }

        @font-face {
            font-family: "omar";
            src: url('../../../fonts/EUROCAPS.ttf');
        }

        * {
            font-family: 'omar';
        }

        body {
            font-family: 'omar';
        }
    </style>
    @yield('styles')

    <script type="text/javascript" src="{{asset('js/jquery.js')}}"></script>

    {!! $openGraph or '' !!}
    {!! $card or '' !!}
    {!! $webmasters or '' !!}
</head>
<body class="stretched" style="">
<div class="loader">
    <img class="text-center" src="{{asset('images/logo-dark-loader.png')}}"  alt="loader">
    <div class="progress-bars">
        <div id="custom-progress" class="custom-progress"></div>
    </div>
</div>
<div id="wrapper" class="clearfix " style="animation-duration: 1.5s; opacity: 1;">
    @include('layouts.header')
    @yield('content')
    {{--    @include('layouts.footer')--}}
</div>
<div id="gotoTop" class="icon-angle-up"></div>

<!-- Scripts -->

<script>
    var dir = "{{$dir}}";
    var _csrf = '{{csrf_token()}}';
    var lang = '{{$lang}}';
    var right = '{{$right}}';
    var left = '{{$left}}';
    var baseUrl = '{{localizeURL('').'/'}}';
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': _csrf
        }
    });
</script>
<script type="text/javascript" src="{{asset('js/plugins.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.nicescroll.js')}}"></script>
<script>
    $('body').niceScroll({
        cursorcolor: "#958e8f",
        cursorwidth: "5px",
        zindex: 9999999,
        background: "rgba(20,20,20,0.7)",
        cursorborder: "1px solid #958e8f",
        cursorborderradius: 10
    });

</script>

<!-- Footer Scripts
============================================= -->
<script type="text/javascript" src="{{asset('js/functions.js')}}"></script>
<script type="text/javascript" src="{{asset('js/custom.js')}}"></script>
@yield('scripts')
{{--<script type="text/javascript" src="{{asset('js/index.js')}}"></script>--}}


</body>
</html>
