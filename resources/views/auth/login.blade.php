@extends('layouts.app')
@section('title',trans('app.login'))
@section('content')
    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">

                <div class="accordion accordion-lg divcenter nobottommargin clearfix" {{(Session::getOldInput('f-name',""))?' data-active=2':''}} style="max-width: 550px;">

                    <div class="acctitle "><i class="acc-closed icon-lock3"></i><i class="acc-open icon-unlock"></i>@lang('app.log_in_to_account')</div>
                    <div class="acc_content  clearfix">
                        <form id="login-form" name="login-form" class="nobottommargin" action="{{localizeURL('login')}}" method="post">
                            {!! csrf_field() !!}
                            {!!Form::thText('col_full','login-form-username','app.username','',true,'name') !!}
                            {!!Form::thText('col_full','login-form-password','app.password','',true,'password',null,null,'password') !!}
                            <div class="col_full hidden">
                                <input type="text" id="botcheck" name="login-form-botcheck" value="" class="" />
                            </div>
                            <div class="col_full">
                                <input name="remember" type="checkbox" value="remember-me"> {{trans('menu.remember')}}
                            </div>
                            <div class="col_full nobottommargin">
                                <button type="submit" class="button button-3d  nomargin " id="login-form-submit" name="login-form-submit" value="login">@lang('app.login')</button>
                            </div>
                        </form>
                    </div>


                </div>

            </div>

        </div>

    </section>
    @include('layouts.footer')
@endsection
@section('scripts')
    <script type="text/javascript" src="{{asset('js/index.js')}}"></script>
    @endsection
