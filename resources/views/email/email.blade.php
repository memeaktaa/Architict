<!DOCTYPE html>
<html class="" dir="{{$dir}}" lang="{{ app()->getLocale() }}">
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css"
         >
</head>
<body dir="{{$dir}}">
{{--<h1>{{trans('app.hi')}} <em style="color: #ea4257">{{$user->name}}</em></h1>--}}
<br>
{{--<p>{{trans('app.your_code')}} <b>{{$code}}</b></p>--}}
<br>
<div><b>Date:</b> {{date('Y-m-d h:i a')}}</div>
<div><b>From:</b> <a href="mailto:{{$user}}">{{$user}}</a></div>
<div><b>Subject:</b> {{$subject}}</div>
<br>
<div><b>Message:</b>{{$content}}</div>
<br>
</body>
</html>
