@extends('layouts.app')
@section('title',trans('app.work'))
@section('content')
    <section id="content" class="clearfix" style="width: 100%">

        <div class="content-wrap">
            <div class="dark clearfix">
                <div class="heading-block dark center noborder">
                    <h3 style="font-family: 'omar';color: #fff6e6">{{trans('app.our_projects')}}</h3>
                    {{--<span>Click on the photography grids</span>--}}
                </div>

                <!-- Grid Items
                ============================================= -->
                <div class="grid">
                    @foreach(\App\Models\Project::all() as $project)
                        <div class="grid-item" data-size="1280x853">
                            <a href="{{$project->image?asset(\App\Models\Project::IMAGE_File_PATH.$project->image):asset('photography/images/items/1.jpg')}}"
                               class="img-wrap img-grayscale" data-animate="fadeInUp">
                                <img src="{{$project->image?asset(\App\Models\Project::IMAGE_File_PATH.$project->image):asset('photography/images/items/1.jpg')}}"
                                     alt="img01"/>
                                <div class="description description-grid">
                                    <h3 style="font-family: 'omar'">{{$project['title_'.$lang]}}</h3>
                                    {{--<p>{!! $project['content_'.$lang] !!}</p>--}}
                                    {{--<div class="details">--}}
                                        {{--<ul>--}}
                                            {{--<li><i class="icon icon-location"></i><span>{{$project->location}}</span>--}}
                                            {{--</li>--}}
                                            {{--<li><i class="icon icon-building"></i><span>{{$project_name->value}}</span>--}}
                                            {{--</li>--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                <!-- /grid -->
                <div class="preview">
                    <button class="action action-close"><i class="icon-line2-close"></i><span
                                class="text-hidden">Close</span></button>
                    <div class="description description-preview"></div>
                </div>
            </div>

            <div class="clear"></div>
        </div>

    </section>
@endsection
@section('styles')
    <link rel="stylesheet" href="{{asset('photography/photography.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('photography/css/photography-addons.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('photography/css/fonts.css')}}" type="text/css"/>
@endsection
@section('scripts')
    <script src="{{asset('photography/js/photography-addons.js')}}"></script>
    <script>
        (function () {
            var support = {transitions: Modernizr.csstransitions},
                // transition end event name
                transEndEventNames = {
                    'WebkitTransition': 'webkitTransitionEnd',
                    'MozTransition': 'transitionend',
                    'OTransition': 'oTransitionEnd',
                    'msTransition': 'MSTransitionEnd',
                    'transition': 'transitionend'
                },
                transEndEventName = transEndEventNames[Modernizr.prefixed('transition')],
                onEndTransition = function (el, callback) {
                    var onEndCallbackFn = function (ev) {
                        if (support.transitions) {
                            if (ev.target != this) return;
                            this.removeEventListener(transEndEventName, onEndCallbackFn);
                        }
                        if (callback && typeof callback === 'function') {
                            callback.call(this);
                        }
                    };
                    if (support.transitions) {
                        el.addEventListener(transEndEventName, onEndCallbackFn);
                    }
                    else {
                        onEndCallbackFn();
                    }
                };

            new GridFx(document.querySelector('.grid'), {
                imgPosition: {
                    x: -0.5,
                    y: 1
                },
                onOpenItem: function (instance, item) {
                    instance.items.forEach(function (el) {
                        if (item != el) {
                            var delay = Math.floor(Math.random() * 50);
                            el.style.WebkitTransition = 'opacity .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1), -webkit-transform .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1)';
                            el.style.transition = 'opacity .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1), transform .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1)';
                            el.style.WebkitTransform = 'scale3d(0.1,0.1,1)';
                            el.style.transform = 'scale3d(0.1,0.1,1)';
                            el.style.opacity = 0;
                        }
                    });
                },
                onCloseItem: function (instance, item) {
                    instance.items.forEach(function (el) {
                        if (item != el) {
                            el.style.WebkitTransition = 'opacity .4s, -webkit-transform .4s';
                            el.style.transition = 'opacity .4s, transform .4s';
                            el.style.WebkitTransform = 'scale3d(1,1,1)';
                            el.style.transform = 'scale3d(1,1,1)';
                            el.style.opacity = 1;

                            onEndTransition(el, function () {
                                el.style.transition = 'none';
                                el.style.WebkitTransform = 'none';
                            });
                        }
                    });
                }
            });
        })();

        jQuery(document).ready(function ($) {
            var swiperSlider = $('.swiper-parent')[0].swiper;
            swiperSlider.enableKeyboardControl();
        });
    </script>
@endsection