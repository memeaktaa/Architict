@extends('layouts.app')
@section('title',trans('app.contact'))
@section('content')
    <section id="content" class="content-margin">
        <div class="content-wrap">

            <div class="" style="margin: 0 5%">
                <div class="col-lg-4">
                    @foreach(\App\Models\Branch::all() as $branch)
                        <div class="">
                            <h3>{{$branch->address}}</h3>
                            <span>Current time: </span>
                            <time class="time" data-utc="true" data-utc-offset="{{$branch->utc_offset}}"></time>
                            <br>
                            <span>E-mail:<a href="mailto:{{$branch->email?$branch->email:'arch.omarnmansour@gmail.com'}}"
                                            class="header-a"> {{$branch->email?$branch->email:'arch.omarnmansour@gmail.com'}}</a></span><br>
                            <span>Phone:<a href="callto:{{$branch->mobile?$branch->mobile:'+90 534 061 5085'}}"
                                           class="header-a"> {{$branch->mobile?$branch->mobile:'+90 534 061 5085'}}</a></span><br>
                        </div>
                    @endforeach
                    <div class="">
                        <div class="title-dotted-border">
                            <h3>{{trans('app.interested')}}<br> {{trans('app.get_in_touch')}}</h3>
                        </div>
                        <br>
                        <div class="contact-widget">
                            <form class="nobottommargin"
                                  action="{{localizeURL('contact')}}" enctype="multipart/form-data" method="post">
                                {{csrf_field()}}
                                <div class="col_full">
                                    <input type="text" id="template-contactform-name" name="name" value="" placeholder="{{trans('app.name')}}"
                                           class="sm-form-control required"/>
                                </div>

                                <div class="col_full">
                                    <input type="email" id="template-contactform-email" name="email" value="" placeholder="{{trans('app.email')}}"
                                           class="required email sm-form-control"/>
                                </div>

                                <div class="clear"></div>

                                <div class="col_full">
                                    <input type="text" id="template-contactform-subject" name="subject" value="" placeholder="{{trans('app.subject')}}"
                                           class="required sm-form-control"/>
                                </div>

                                <div class="col_full">
                                    <textarea class="required sm-form-control" id="template-contactform-message" placeholder="{{trans('app.message')}}"
                                              name="message" rows="2" cols="30"></textarea>
                                </div>


                                <div class="col_full">
                                    <button type="submit" id="form-submit"
                                            class="button button-border button-circle button-dark">{{trans('app.send')}}</button>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
                <div class="col-lg-8 topmargin col_last hidden-xs hidden-sm" style="min-height: 350px">
                    <img id="chart-radar" src="{{asset('images/map.jpg')}}" alt="world-map" style="position: fixed"/>
                </div>
            </div>
        </div>
    </section>
    @include('layouts.footer')
@endsection
@section('styles')
    <style>
        h3 {
            font-family: 'omar';
        }
    </style>
    @endsection
@section('scripts')
    <script type="text/javascript" src="{{asset('js/jquery.jclock.js')}}"></script>
    <script type="text/javascript"
            src="https://maps.google.com/maps/api/js?key=AIzaSyABPIJkYFCSW_1q2toMCQQviExc7y1ucrQ&language={{$lang}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.gmap.js')}}"></script>
    <script>
        $('#form-submit').on('click',function (e) {
            e.preventDefault();
            var form=$(this).closest('form');
            var url=form.attr('action');
            var data=form.serialize();
            $.ajax({
                type:"POST",
                url:url,
                data:data,
                success:function (data) {
                    SEMICOLON.widget.notifications($('<div id="" data-notify-position="top-right" data-notify-type="info" data-notify-msg="<i class=\'icon-info-sign\'></i>  ' + data.msg + '"></div>'));

                }
            })
        })
        // $('form').ajaxForm(function (data) {
        //     SEMICOLON.widget.notifications($('<div id="" data-notify-position="top-right" data-notify-type="info" data-notify-msg="<i class=\'icon-info-sign\'></i>  ' + data.msg + '"></div>'));
        // });
        $(function () {
            var style = [
                {
                    "elementType": "geometry", "stylers": [
                        {"saturation": -100}
                    ]
                }
            ];

            {{--var map;--}}
            {{--map = new GMaps({--}}
            {{--div: '#google-map',--}}
            {{--zoom: 16,--}}
            {{--lat: 41.0175449,--}}
            {{--lng: 28.6463013,--}}
            {{--maptype: 'ROADMAP',--}}
            {{--zoom: 15,--}}
            {{--featureType: "poi",--}}
            {{--elementType: "labels",--}}
            {{--stylers: [--}}
            {{--{visibility: "off"}--}}
            {{--],--}}
            {{--backgroundColor: "#eeeeee",--}}
            {{--});--}}
            {{--var noPoi = [--}}
            {{--{--}}
            {{--featureType: "poi",--}}
            {{--stylers: [--}}
            {{--{visibility: "off"}--}}
            {{--]--}}
            {{--}--}}
            {{--];--}}
            {{--map.drawOverlay({--}}
            {{--latitude: 41.0175449,--}}
            {{--longitude: 28.6463013,--}}
            {{--content: '<div class="overlay">OMA</div>'--}}
            {{--});--}}
            {{--var icon = {--}}
            {{--url: "{{asset('images/Tab Logo.png')}}", // url--}}
            {{--scaledSize: new google.maps.Size(70, 70), // scaled size--}}
            {{--origin: new google.maps.Point(0, 0), // origin--}}
            {{--anchor: new google.maps.Point(0, 0) // anchor--}}
            {{--};--}}
            {{--map.addMarker({--}}
            {{--lat: 41.0175449,--}}
            {{--lng: 28.6463013,--}}
            {{--icon: icon,--}}
            {{--infoWindow: {--}}
            {{--content: '<div class="">' +--}}
            {{--'<h4 style="margin-bottom: 8px;"><span>OMA DESIGN</span> Group</h4>' +--}}
            {{--'<h3 style="margin-bottom: 8px;"><span>Turkey,</span> Istanbul</h3>' +--}}
            {{--'<p class="">Cumhuriyet Mahallesi Atatürk <br>blv. 341515 Esenyurt/Istanbul</p>' +--}}
            {{--'<span>Current time in Istanbul</span><br><time class="time" data-utc="true" data-utc-offset="3"></time>' +--}}
            {{--'</div>',--}}
            {{--}--}}
            {{--});--}}
            $('.time').each(function () {

                var myUtc = $(this).data('utc');
                var myUtcOffset = $(this).data('utc-offset');

                $(this).jclock({
                    utc: myUtc,
                    utcOffset: myUtcOffset
                })

            });
            // map.setOptions({styles: noPoi});
            // $('#close').click(function () {
            //     $('.map-details').fadeOut('2000');
            //     $('#google-map').css('height', '100vh');
            // })
        });
    </script>
@endsection
