<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon"
          href="{{isset($favicon)?asset(\App\Models\CmpGeneral::IMAGE_File_PATH.$favicon->value):asset('images/favicon.png')}}"
          type="image/x-icon">
    <meta name="theme-color" content="#262626"/>
    <title>{{isset($project_name)?$project_name->value:'OMA'}} {{--| @yield('title')--}}</title>

    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}" type="text/css"/>
    <link rel="stylesheet" property="stylesheet" href="{{asset('css/components.css')}}">
    <link rel="stylesheet" property="stylesheet" href="{{asset('css/custom.css')}}">
    <link rel="stylesheet" property="stylesheet" href="{{asset('css/style-tero.css')}}">
    <link rel="stylesheet" property="stylesheet" href="{{asset('css/gray1.min.css')}}">
    <link rel="stylesheet" property="stylesheet" href="{{asset('css/icon-tero.css')}}">
    <link rel="stylesheet" property="stylesheet" href="{{asset('css/font-icons.css')}}">
    <style>
        ::-webkit-scrollbar {
            width: 12px;
        }

        ::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            border-radius: 10px;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 10px;
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5);
        }
        @font-face {
            font-family: "omar";
            src: url('../../../css/fonts/EUROCAPS.ttf');
        }

        @font-face {
            font-family: "omar";
            src: url('../../../fonts/EUROCAPS.ttf');
        }

        * {
            font-family: 'omar';
        }

        body {
            font-family: 'omar';
        }
    </style>
    <!-- Внешние js файлы-->
    <script src="{{asset('js/jquery.js')}}"></script>

    <script>
        var dir = "{{$dir}}";
        var _csrf = '{{csrf_token()}}';
        var lang = '{{$lang}}';
        var right = '{{$right}}';
        var left = '{{$left}}';
        var baseUrl = '{{localizeURL('').'/'}}';
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': _csrf
            }
        });
    </script>
    <script src="{{asset('js/functions.js')}}"></script>
    <script src="{{asset('js/plugins.js')}}"></script>
    <script src="{{asset('js/libs.js')}}"></script>
    <script src="{{asset('js/components.js')}}"></script>
    <script src="{{asset('js/jquery.gray.min.js')}}"></script>
    <script type='text/javascript' src='{{asset('js/initseff2.js?ver=4.7.9')}}'></script>
    <script type="text/javascript" src="{{asset('js/jquery.nicescroll.js')}}"></script>
    <script src="{{asset('js/jquery.lazyloadxt.js')}}"></script>
{{--    <script src="{{asset('js/index.js')}}"></script>--}}
    <script>
        $(document).ready(function () {
            $(window).lazyLoadXT();
        });
    </script>
</head>
<body class="stretched" style="color: #555">
<div class="loader">
    <img class="text-center" src="{{asset('images/logo1.png')}}" alt="loader">
    <div class="progress-bars">
        <div id="custom-progress" class="custom-progress"></div>
    </div>
</div>
<div class="" id="wrapper" style="">
    <header id="header" class="full-header transparent-header">

        <div id="header-wrap">

            <div class="container clearfix @if(URL::current()==route('home') || URL::current()==route('/')) t @else white-header @endif">

                <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                <!-- Logo
                ============================================= -->
                <div id="logo" style="border-right:none !important; ">
                    <a href="{{route('home')}}" class="standard-logo"
                       style="margin-top: 10% !important;color: @if(URL::current()==route('home') ||URL::current()==route('/')) white @endif"><b>{{$project_name->value}}</b></a>
                    <a href="{{route('home')}}" class="retina-logo"
                       style="margin-top: 5%;color: @if(URL::current()==route('home') ||URL::current()==route('/')) white @endif">
                        <b class="project-name">{{$project_name->value}}</b></a>
                </div><!-- #logo end -->

                <!-- Primary Navigation
                ============================================= -->
                <nav id="primary-menu" class="dark">

                    <ul class="@if(URL::current()==route('home') || URL::current()==route('/')) show @endif"
                        style="border-right: none !important;">

                        <li class=" " style="height: 40px">
                            @if($facebook)
                                <a href="https://www.facebook.com/{{$facebook->value}}" target="_blank"
                                   class="social-icon si-rounded si-borderless icon-facebook"
                                   style="padding: 2px 11px;margin: 32px 0px;">
                                    <div hidden>
                                        <i class="icon-facebook" style="color:#575757;"></i>
                                        <i class="icon-facebook"></i>
                                    </div>
                                </a>
                            @endif
                            @if($linkedin)
                                <a href="https://www.linkedin.com/{{$linkedin->value}}" target="_blank"
                                   class="social-icon si-rounded si-borderless icon-linkedin2"
                                   style="padding: 2px 11px;margin: 32px 0px;">
                                    <div hidden>
                                        <i class="icon-linkedin" style="color:#575757;"></i>
                                        <i class="icon-linkedin"></i>
                                    </div>
                                </a>
                            @endif
                            @if($instagram)
                                <a href="https://www.instagram.com/{{$instagram->value}}" target="_blank"
                                   class="social-icon si-rounded si-borderless icon-instagram"
                                   style="padding: 2px 11px;margin: 32px 0px;">
                                    <div hidden>
                                        <i class="icon-instagram" style="color:#575757;"></i>
                                        <i class="icon-instagram"></i>
                                    </div>
                                </a>
                            @endif
                        </li>

                        <li class=" "><a
                                    class=""
                                    href="{{route('/')}}"
                                    style="color: @if(URL::current()==route('home') ||URL::current()==route('/')) white @else #0a1115 @endif;font-weight: 800">
                                <div>{{trans('app.home')}}</div>
                            </a>
                        </li>
                        <li class=" "><a class=""
                                         href="{{route('page',['page'=>'work'])}}"
                                         style="color: @if(URL::current()==route('home') ||URL::current()==route('/')) white @else #0a1115 @endif;font-weight: 800">
                                <div>{{trans('app.work')}}</div>
                            </a>
                        </li>
                        <li class=" "><a class=""
                                         href="{{route('page',['page'=>'about-us'])}}"
                                         style="color: @if(URL::current()==route('home') ||URL::current()==route('/')) white @else #0a1115 @endif;font-weight: 800">
                                <div>{{trans('app.about')}}</div>
                            </a>
                        </li>
                        <li class=" "><a class=""
                                         href="{{route('page',['page'=>'contact'])}}"
                                         style="color: @if(URL::current()==route('home') ||URL::current()==route('/')) white @else #0a1115 @endif;font-weight: 800">
                                <div>{{trans('app.contact')}}</div>
                            </a>
                        </li>
                        <li class=" "><a class="" href="{{LaravelLocalization::getLocalizedURL($lang=='en'?'tr':'en') }}"
                                         {{--{{ LaravelLocalization::getLocalizedURL($lang=='en'?'tr':'en') }}--}}
                                         hreflang="{{ $lang=='en'?'TR':'EN' }}"
                                         style="color: @if(URL::current()==route('home') ||URL::current()==route('/')) white @else #0a1115 @endif;font-weight: 800">
                                <div>{{$lang=='en'?'TR':'EN'}}</div>
                            </a>
                        </li>
                    </ul>
                </nav><!-- #primary-menu end -->

            </div>

        </div>

    </header>
    <!-- .wHeader-->
    <section id="content" class="content-margin" style="width: 100%">
        <div class="content-wrap work curr" style="padding-top: 0px !important">
            <div class="tetris" data-offset="10">
                @foreach(\App\Models\Project::paginate(10) as $key=>$project)
                    <div data-sort="{{$key}}" data-id="{{asset(\App\Models\Project::IMAGE_File_PATH.$project->image)}}"
                         data-src="{{asset(\App\Models\Project::IMAGE_File_PATH.$project->image)}}"
                         data-sub-html="{!!$project['title_'.$lang]!!}"
                         data-pinterest-text="{{$project['title_'.$lang]}}"
                         data-facebook-text="{{$project['title_'.$lang]}}"
                         data-tweet-text="{{$project['title_'.$lang]}}" class="tetris__item ">
                        <!--<div style="background-image:url(http://tero.design/wp-content/uploads/2017/08/3_Interactive-LightMix.jpg)"></div>
                        <div style="background-image:url(http://tero.design/wp-content/uploads/2017/08/3_Interactive-LightMix.jpg)" class="grayImg"></div>-->
                        <img class="grayscale grayscale-fade img-responsive"
                             data-src="{{asset(\App\Models\Project::IMAGE_File_PATH.$project->image)}}"
                             src="{{asset('images/preloader1.gif')}}" width="400" height="600" alt="">
                    </div>
                    @if($loop->last)
                        <input value="{{$project->id}}" id="num-ele" hidden>
                        <div id="id-of-element" hidden
                             data-id="{{$project->id}}">{{$project->id}}</div>
                    @endif
                @endforeach
            </div>
        </div>
        <!-- .wFooter-->
    </section>
    @include('layouts.footer')
    <script>
        $(document).ready(function () {
            var offsetPost = $('#id-of-element').text();
            var offsetNum = $('#num-ele').val();
            var ajax = false;
            $(window).on("scroll", function () {
                var cur = $(".work").hasClass("curr");
                var $container = $('.tetris');

                if(cur) {

                    var winH = $(window).height();
                    var tet = $('.tetris');

                    var offset = tet.offset();
                    var top = offset.top;
                    var bottom = top + tet.outerHeight();
                    var pos = $(document).scrollTop();

                    var offsetPost = parseInt($('.tetris').attr('data-offset'));
                    if ((pos + winH + 350 > bottom) && !ajax) {
                        ajax = true;
                        count = 0;
                        var data = {
                            offset: offsetNum ? offsetNum : offsetPost
                        };
                        var url = baseUrl + 'projects';
                        jQuery.get(url, data, function (response) {
                            for (var i = 0; i < response.length; i++) {
                                var resp = response[i];
                                var size = '';
                                var $newItems = '';
                                $newItems = $('<div data-order="' + resp.id + '" data-id="' + resp.link + '" data-src="' + resp.link + '" data-sub-html="' + resp.name + '" data-pinterest-text="' + resp.name + '" data-tweet-text="' + resp.name + '" class="tetris__item ' + size + '"><img class="grayscale grayscale-fade" src="' + resp.link + '"></div>');
                                $container.append($newItems).isotope('appended', $newItems);
                                offsetPost = resp.id;
                                offsetNum = resp.id;
                                $('.tetris').attr('data-offset', resp.id)
                            }
                            $(window).trigger("resize");
                            $("body").getNiceScroll().resize();
                            // $(window).bind('scroll');
                            ajax = true;
                            $('.tetris').attr('data-offset', offsetPost);
                        }).done(function () {
                            ajax = false;
                            // $(window).trigger("resize");
                            // $("body").getNiceScroll().resize();
                            setTimeout(function () {
                                //$(window).trigger("resize");
                                $container.isotope('layout');
                            }, 50);
                            var $lg = $('.tetris');
                            $lg.data('lightGallery').destroy(true);
                            $lg.lightGallery({
                                autoplay: false,
                                zoom: false,
                                download: false,
                                twitter: false,
                                googlePlus: false
                            });
                        });
                    }
                }
            });
        });
        $(document).ajaxStop(function () {
            if ($.fn.lightGallery() !==undefined){
                $('.lg-close').bind('click.lg');
                $('.lg-next').bind('click.lg');
                $('.lg-prev').bind('click.lg');

            }
        })
    </script>
</div>
<script src="{{asset('js/custom.js')}}"></script>
</body>

</html>