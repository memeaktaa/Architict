@extends('layouts.app')
@section('title',trans('app.about_us'))
@section('content')
    <section id="content" class="content-margin" style="width: 100%">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="col_full">
                    <div class="heading-block center nobottomborder">
                        <h2 style="font-family: 'omar'">{{trans('app.who_we_are')}}</h2>
                        <p style="font-family: Arial !important;">{!! $info['content_'.$lang] !!}</p>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="heading-block center">
                    <h2 style="font-family: 'omar'">{{trans('app.our_services')}}</h2>
                </div>

                <div class="row center nobottommargin">
                    {{--<ul style="" class="text-center iconlist">--}}
                    {{--@foreach(\App\Models\Project::all() as $project)--}}
                    {{--<li>{{$project['title_'.$lang]}}</li>--}}
                    {{--@endforeach--}}
                    {{--</ul>--}}
                    <p style="text-align: center;font-size: 18px;font-family: Arial !important;">
                        @foreach(\App\Models\Service::all() as $service)
                            {{$service['title_'.$lang]}}<br>
                        @endforeach
                    </p>
                </div>
                <div class="clear"></div>

                <div class="heading-block center">
                    <h4>{{trans('app.our_clients')}}</h4>
                </div>

                <ul id="clients" class="clients-grid grid-8 nobottommargin clearfix">
                    @foreach(\App\Models\Client::where('active','Y')->get() as $client)
                        <li><a href="#clients"><img class="grayscale"
                                                    src="{{$client->logo?asset(\App\Models\Client::LOGO_File_PATH.$client->logo):asset('')}}"
                                                    alt="Clients"/></a></li>
                    @endforeach
                </ul>
            </div>
        </div>

    </section>
    @include('layouts.footer')
@endsection

