<div class="{{$contClass}}">
    <button class="f{{$right}} button  {{$class}} nomargin" type="submit" id="{{$id}}" name="{{$id}}" value="submit">@lang($label)</button>
</div>