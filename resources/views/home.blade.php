@extends('layouts.app')
@section('title',trans('app.home'))
@section('content')
    @include('layouts.slider')
    <section id="content" class="hidden-xs hidden-sm" style="width: 100vh;position: absolute;top: 40%;left: 40%;background-color: transparent;overflow-y: hidden">
        <img class="img-responsive" style="width: 45%;overflow-y: hidden" src="{{asset('images/logo-word.png')}}" alt="logo"/>
    </section>
@endsection
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('include/css/settings.css')}}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{asset('include/css/layers.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('include/css/navigation.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style-modified.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/responsive-modified.css')}}">
    @endsection
@section('scripts')
    <script type="text/javascript" src="{{asset('include/js/jquery.themepunch.tools.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('include/js/jquery.themepunch.revolution.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('include/js/extensions/revolution.extension.video.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('include/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('include/js/extensions/revolution.extension.actions.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('include/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('include/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('include/js/extensions/revolution.extension.navigation.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('include/js/extensions/revolution.extension.migration.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('include/js/extensions/revolution.extension.parallax.min.js')}}"></script>

    <script type="text/javascript">

        // var jQuery=jQuery;
        // jQuery.noConflict();

        $(document).ready(function() {

            var apiRevoSlider = $('.tp-banner').show().revolution(
                {
                    sliderType:"standard",
                    jsFileLocation:baseUrl+"include/js/",
                    dottedOverlay:"none",
                    delay:16000,
                    startwidth:1140,
                    startheight:600,
                    hideThumbs:200,

                    thumbWidth:100,
                    thumbHeight:50,
                    thumbAmount:5,

                    navigation: {
                        keyboardNavigation: "on",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation: "off",
                        onHoverStop: "off",
                        touch: {
                            touchenabled: "on",
                            swipe_threshold: 75,
                            swipe_min_touches: 1,
                            swipe_direction: "horizontal",
                            drag_block_vertical: false
                        },
                        arrows: {
                            style: "hades",
                            enable: true,
                            hide_onmobile: false,
                            hide_onleave: false,
                            tmp: '<div class="tp-arr-allwrapper">	<div class="tp-arr-imgholder"></div></div>',
                            left: {
                                h_align: "left",
                                v_align: "center",
                                h_offset: 10,
                                v_offset: 0
                            },
                            right: {
                                h_align: "right",
                                v_align: "center",
                                h_offset: 10,
                                v_offset: 0
                            }
                        },
                    },

                    touchenabled:"on",
                    onHoverStop:"on",

                    swipe_velocity: 0.7,
                    swipe_min_touches: 1,
                    swipe_max_touches: 1,
                    drag_block_vertical: false,

                    parallax:"mouse",
                    parallaxBgFreeze:"on",
                    parallaxLevels:[7,4,3,2,5,4,3,2,1,0],

                    keyboardNavigation:"off",

                    navigationHAlign:"center",
                    navigationVAlign:"bottom",
                    navigationHOffset:0,
                    navigationVOffset:20,

                    soloArrowLeftHalign:"left",
                    soloArrowLeftValign:"center",
                    soloArrowLeftHOffset:20,
                    soloArrowLeftVOffset:0,

                    soloArrowRightHalign:"right",
                    soloArrowRightValign:"center",
                    soloArrowRightHOffset:20,
                    soloArrowRightVOffset:0,

                    shadow:0,
                    fullWidth:"off",
                    fullScreen:"on",

                    spinner:"spinner4",

                    stopLoop:"off",
                    stopAfterLoops:-1,
                    stopAtSlide:-1,

                    shuffle:"off",

                    autoHeight:"off",
                    forceFullWidth:"off",



                    hideThumbsOnMobile:"off",
                    hideNavDelayOnMobile:1500,
                    hideBulletsOnMobile:"off",
                    hideArrowsOnMobile:"off",
                    hideThumbsUnderResolution:0,

                    hideSliderAtLimit:0,
                    hideCaptionAtLimit:0,
                    hideAllCaptionAtLilmit:0,
                    startWithSlide:0,
                });

            apiRevoSlider.bind("revolution.slide.onloaded",function (e) {
                setTimeout( function(){ SEMICOLON.slider.sliderParallaxDimensions(); }, 200 );
            });

            apiRevoSlider.bind("revolution.slide.onchange",function (e,data) {
                SEMICOLON.slider.revolutionSliderMenu();
            });

        }); //ready

    </script>
    @endsection
