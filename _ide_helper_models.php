<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Attribute
 *
 * @property int $attribute_id
 * @property string $text
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attribute whereAttributeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attribute whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attribute whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attribute whereUpdatedAt($value)
 */
	class Attribute extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Attribute_page
 *
 * @property int $id
 * @property int $page_id
 * @property int $attribute_id
 * @property string|null $attribute_value
 * @property string|null $image
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attribute_page whereAttributeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attribute_page whereAttributeValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attribute_page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attribute_page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attribute_page whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attribute_page wherePageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Attribute_page whereUpdatedAt($value)
 */
	class Attribute_page extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CmpGeneral
 *
 * @property int $general_id
 * @property string $type
 * @property string|null $value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CmpGeneral whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CmpGeneral whereGeneralId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CmpGeneral whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CmpGeneral whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CmpGeneral whereValue($value)
 */
	class CmpGeneral extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ContactMail
 *
 * @property int $id
 * @property string|null $f_name
 * @property string|null $l_name
 * @property string|null $phone
 * @property string|null $email
 * @property string|null $message
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactMail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactMail whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactMail whereFName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactMail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactMail whereLName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactMail whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactMail wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ContactMail whereUpdatedAt($value)
 */
	class ContactMail extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Gallery
 *
 * @property int $id
 * @property string|null $title_en
 * @property string|null $title_tr
 * @property string|null $content_en
 * @property string|null $content_tr
 * @property string|null $image
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery whereContentEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery whereContentTr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery whereTitleEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery whereTitleTr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gallery whereUpdatedAt($value)
 */
	class Gallery extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Page
 *
 * @property int $page_id
 * @property string $page_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page wherePageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page wherePageName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereUpdatedAt($value)
 */
	class Page extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Project
 *
 */
	class Project extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property int $university_id
 * @property int $user_type_id
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUniversityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUserTypeId($value)
 * @mixin \Eloquent
 * @property string|null $phone
 * @property int $is_admin
 * @property string|null $image
 * @property string|null $account_money
 * @property string|null $is_verified
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAccountMoney($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIsAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIsVerified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhone($value)
 */
	class User extends \Eloquent {}
}

