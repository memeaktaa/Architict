<?php

/**
 *  you must add here route and model and view foreach
 *
 *  model             : you must add model for datatable
 *  dataTableFunc     : default funcName is get_datatable // add name for maker method ,this name will be used in maker class or in factory class as get_func / build_func / store_func / update_fund / destroy_func
 *  middlewares       : you can make auth by pass string or array to this property
 *  middlewaresOption : this option allow you to only make auth on on action or except action from auth by pass array
 *  request           : this is must be Form Request Class
 *  stopOperation     : for stop normal oper and add custom oper ['store','update','destroy']
 *  factory           : the default factory is  modelFactory unless you add you own factory property
 *
 */

use App\User;

return [

    'gallery' => [
        'model' => \App\Models\Gallery::class,
        'factory' => \App\Factories\GalleryFactory::class,
    ],
    'projects' => [
        'model' => \App\Models\Project::class,
        'factory' => \App\Factories\ProjectFactory::class,
    ],
    'clients' => [
        'model' => \App\Models\Client::class,
        'factory' => \App\Factories\ClientFactory::class,
    ],
    'team' => [
        'model' => \App\Models\Team::class,
        'factory' => \App\Factories\TeamsFactory::class,
    ],
    'attributes' => [
        'model' => \App\Models\Attribute::class,
        'factory' => \App\Factories\AttributeFactory::class,
    ],
    'attribute-page' => [
        'model'      => \App\Models\Attribute_page::class,
        'factory'    => \App\Factories\AttributePageFactory::class,
    ],
    'generals' => [
        'model'      => \App\Models\CmpGeneral::class,
        'factory'    => \App\Factories\GeneralFactory::class,
    ],
    'pages' => [
        'model' => \App\Models\Page::class,
        'factory' => \App\Factories\PageFactory::class,
        'stopOperation' => ['store', 'update', 'destroy']
    ],
    'branches' => [
        'model' => \App\Models\Branch::class,
        'factory' => \App\Factories\BranchFactory::class,
    ],
    'services' => [
        'model' => \App\Models\Service::class,
        'factory' => \App\Factories\ServiceFactory::class,
    ],
    'contact-mails' => [
        'model' => \App\Models\ContactMail::class,
        'factory' => \App\Factories\ContactMailFactory::class,
    ],
    'visitors' => [
        'model' => \App\Models\Tracker::class,
        'factory' => \App\Factories\TrackerFactory::class,
    ],

];
