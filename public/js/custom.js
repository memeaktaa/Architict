function progressloader() {
    var progress = $('#custom-progress');
    var numProgress = 5;
    var id = setInterval(frame, 75);

    function frame() {
        if (numProgress === 100) {
            clearInterval(id);
            progress.css('width','0%');
            numProgress=5;
            progressloader();
        }
        else {
            numProgress += 5;
            progress.css('width', numProgress + '%');
        }
    }
}

progressloader();
$(window).load(function () {
    $('.loader').fadeOut('slow', function () {
        $(this).remove();
    });
});