/*init.js*/
jQuery(document).ready(function () {

    var $container = $('.tetris');
    delim = 5;
    if (Modernizr.mq('screen and (max-width: 1600px)')) {
        delim = 4;
    }

    if (Modernizr.mq('screen and (max-width: 770px)')) {
        delim = 3;
    }
    if (Modernizr.mq('screen and (max-width: 440px)')) {
        delim = 2;
    }

    $container.isotope({
        layoutMode: 'packery',
        itemSelector: '.tetris__item',
        packery: {
            columnWidth: $container.width() / delim
        },
        getSortData: {
            category: '[data-sort] parseInt'
        },
        sortBy: 'category'
    });

    var wwidth = $('html').width();

    var t = moment();
    var time = t.tz("Europe/Warsaw").format("H:mm");
    var tel = $(".map__block__time span");
    tel.text(time);

    setInterval(function () {
        var time = moment().tz("Europe/Warsaw").format("H:mm");
        tel.text(time);
    }, 10000);

    $('.mfi').on("click", function () {
        var el = $(this);
        var d = el.find('.tetrisQuestions__item__popup').html();
        $.magnificPopup.open({
            items: {
                src: d,
                type: 'inline'
            },
            type: 'inline',
            closeBtnInside: true,
            removalDelay: 300,
            mainClass: 'zoom-in'
        })
    });

    $('.js-work').on("click", function (e) {
        e.preventDefault();
        var current = $('.curr');
        if (!$(".work").hasClass('curr')) {
            $('.work').addClass('curr');

            if ($('.mobile__overlay').hasClass('opened')) {
                $('.mobile__overlay').removeClass('opened')
            }

            current.stop().fadeOut(400, function () {
                $('.loaderGal').stop().fadeIn(400, function () {
                    $(".work").show();
                    $container.isotope('layout');
                    $(window).trigger("resize");
                    $('.wFooter').show();
                    setTimeout(function () {
                        $('.loaderGal').stop().fadeOut();
                    }, 400);
                });

            });
            current.removeClass('curr');
        }
    });

    $('.js-close-cont').on("click", function () {
        var el = $(this);
        $('.js-form').find('input').val('');
        $('.js-form').find('input').removeClass("upper correct");
        $('.js-form').find('textarea').val('');
        $('.js-form').find('textarea').removeClass("upper correct");
        setTimeout(function () {
            el.closest(".loaderbg2").fadeOut();
        }, 200);
    });

    $('.js-about').on("click", function (e) {
        e.preventDefault();
        var current = $('.curr');
        if (!$(".about").hasClass('curr')) {
            $('.about').addClass('curr');

            if ($('.mobile__overlay').hasClass('opened')) {
                $('.mobile__overlay').removeClass('opened')
            }

            current.stop().fadeOut(400, function () {
                $('.loaderGal').stop().fadeIn(400, function () {
                    $(".about").show();
                    $(window).trigger("resize");
                    $('.wFooter').show();
                    setTimeout(function () {
                        $('.loaderGal').stop().fadeOut();
                    }, 400);
                });

            });
            current.removeClass('curr');
        }
    });

    $('.js-contacts').on("click", function (e) {
        e.preventDefault();
        var current = $('.curr');
        if (!$(".map").hasClass('curr')) {
            $('.map').addClass('curr');
            $('.wFooter').hide();

            if ($('.mobile__overlay').hasClass('opened')) {
                $('.mobile__overlay').removeClass('opened')
            }

            current.fadeOut(400, function () {
                $('.loaderGal').stop().fadeIn(400, function () {
                    $(".map").show();

                    if ($("#map").length) {
                        initMap();
                        codeAddress();

                    }
                    setTimeout(function () {
                        $('.loaderGal').stop().fadeOut();
                    }, 400);
                });
            });
            current.removeClass("curr");
        }
    });

    $('.js-dropdown').on("click", function () {
        var el = $(this);

        if ($('.js-dropdown').not(this).hasClass("droped")) {
            var el2 = $('.js-dropdown.droped');
            if (el != el2) {
                el2.children(".js-list").stop().fadeOut();
                el2.removeClass("droped");
            }
        }

        if (el.hasClass('droped')) {
            el.children(".js-list").stop().fadeOut();
            el.removeClass("droped");
        } else {
            el.children(".js-list").stop().fadeIn();
            el.addClass("droped");
        }
    });

    $('.js-burger').on('click', function () {
        $('.mobile__overlay').addClass("opened");
    });
    $('.js-menu-close').on("click", function () {
        $('.mobile__overlay').removeClass("opened");
    });
    $('.js-contact-form').on('click', function () {
        $('.contactform').addClass("opened");
        $('.mobile__overlay').removeClass("opened");
    });
    $('.js-contform-close').on("click", function () {
        $('.contactform').removeClass("opened");
    });


    $(window).resize(function () {
        wwidth = $('html').width();

        var $container = $('.tetris');
        delim = 5;
        if (Modernizr.mq('screen and (max-width: 1600px)')) {
            delim = 4;
        }
        if (Modernizr.mq('screen and (max-width: 770px)')) {
            delim = 3;
        }
        if (Modernizr.mq('screen and (max-width: 440px)')) {
            delim = 2;
        }

        $container.isotope({
            packery: {
                columnWidth: $container.width() / delim
            }
        });

        setTimeout(function () {
            $container.isotope('layout');
        }, 150);
    });

    $(".loader").addClass("anim");
    $(".loaderLogo").addClass("anim");
    var ajax = false;


    $('.map__block__form__row input, .map__block__form__row textarea').on('blur', function () {
        var el = $(this);

        if (el.val().length > 0) {
            el.addClass("upper");
        } else {
            el.removeClass("upper");
        }

    });

    function correct(el) {
        el.addClass("correct");
        el.removeClass("error");
        el.next().next("b").html("&#10004;");
        el.next().next("b").removeAttr("style");
    }

    function error(el) {
        el.addClass("error");
        el.removeClass("correct");
        el.next().next("b").html("!");
        el.next().next("b").css("fontWeight", 700);
    }

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    $("body").on('keyup', ".js-mask-name", function () {
        var el = $(this);
        if (el.val().match('^[a-zA-Z]{2,16}$')) {
            correct(el);
        } else {
            error(el);
        }
    });

    $("body").on('keyup', ".js-mask-length", function () {
        var el = $(this);
        var re = /.{10,}/g;
        var count = parseInt(el.data('length'));
        if (el.val().match(re)) {
            correct(el);
        } else {
            error(el);
        }
    });

    $("body").on('keyup', ".js-mask-email", function () {
        var el = $(this);
        if (validateEmail(el.val())) {
            correct(el);
        } else {
            error(el);
        }
    });


    $('.map__block__form input + span,.map__block__form textarea + span').on('click', function () {
        var el = $(this);
        el.prev('input,textarea').trigger("focus");
    });

    $('.grayscale').gray();


    $(window).load(function () {
        $(window).trigger("resize");
        setTimeout(function () {
            $(window).trigger("resize");
        }, 100);
        setTimeout(function () {
            $(".loaderbg").fadeOut();
        }, 400);
        var $customEvents = $(".tetris");
        $customEvents.lightGallery({
            autoplay: false,
            zoom: false,
            download: false,
            twitter: false,
            googlePlus: false
        });

        $customEvents.on('onBeforeSlide.lg', function (event, prevIndex, index) {
            //$('body').find('.lg-item').eq(index).addClass('loop');
            //$('body').find('.lg-item').eq(index).children('.lg-img-wrap').css('opacity','0');
        });


        $customEvents.on('onSlideItemLoad.lg', function (event, prevIndex, index) {
            //$('.lg-current').removeClass('loop');
            // $('.lg-current').children('.lg-img-wrap').removeAttr('style');
            // $('.lg-current').css('background','none');
        });

        $customEvents.on('onAfterSlide.lg', function (event, prevIndex, index) {
            var el = $('.tetris__item').eq(index);
            if (el[0] !== undefined) {

                var permalink = el[0].dataset.id;
                var src = el[0].dataset.src;
                var desc = el[0].dataset.subHtml;
                // desc = $(desc).text();

                var posturl = "https://www.facebook.com/sharer/sharer.php?u=" + encodeURIComponent(permalink);
                // var posturlInsta = "https://www.instagram.com/p/"+encodeURIComponent(permalink);
                var posturlPint = "http://www.pinterest.com/pin/create/button/?url=" + encodeURIComponent(permalink) + "&media=" + src + "&description=" + desc;
                $('body').find('#lg-share-facebook').attr("href", posturl);
                $('body').find('#lg-share-pinterest').attr("href", posturlPint);
                // $('body').find('#lg-share-instagram').attr("href",posturlInsta);
            }
        });

    });


    var img99 = $("img[src='http://tero.design/wp-content/uploads/2017/02/1.gif']");
    if (img99.length) {
        console.log("+");
        var p99 = img99.parent();
        var text99 = "<p>We where thinking for a long time how to <br>introduce ourselves to the client here</p><p>but decided not to write anything</p><p>We are not skillfull in an eloquent writing</p><p>but we are chosen not because of this ...</p>";
        p99.html(text99);
        p99.css("height", "275px").css("text-align", "center").css("display", "table").css("width", "100%")
            .css("font-size", "45px").css("font-weight", "100").css("line-height", "1.5");
        p99.children().hide().css("display", "table-cell").css("vertical-align", "middle");
        i = 1;
        setInterval(function () {
            p99.children().hide();
            p99.children(":nth-child(" + i + ")").show();
            i++;
            if (i == 5) i = 1;
        }, 5000);

    }

});
